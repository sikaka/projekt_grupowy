﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Resources;
using System.Device.Location;
using System.Text.RegularExpressions;
using System.Windows.Navigation;
using System.Windows.Threading;


namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
        GeoCoordinate coord;
        private int counter;
        DispatcherTimer timer = new DispatcherTimer();
        private double lat;
        private double lon;

        public StartWindow()
        {
            InitializeComponent();
            counter = 0;
           // comunicat.Text = "Wait for GPS...";
           // openStreetMap.IsEnabled = false;
            if (tryLocalisation() == true)
            {
                comunicat.Text = "GPS Ready";
                lat = coord.Latitude;
                lon = coord.Longitude;
            }
            else
            {
                comunicat.Text = "Wait for GPS...";
                openStreetMap.IsEnabled = false;
            }
            
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
           // timer.Start();

            comunicat.Text = Environment.OSVersion.ToString();

        }
        private void timer_Tick(object sender, EventArgs e)
        {
            if (counter != 10)
            {
                isGPSactive();
                counter++;
            }
            else
            {
                timer.Stop();
                comunicat.Text = "No GPS";
            }
        }
        private void isGPSactive()
        {
            if (tryLocalisation() == true)
            {
                comunicat.Text = "GPS Ready";
                openStreetMap.IsEnabled = true;
                timer.Stop();
                lat = coord.Latitude;
                lon = coord.Longitude;
            }
            else
            {
                comunicat.Text = "Wait for GPS...";
                openStreetMap.IsEnabled = false;
            }
        }

        private void openMap_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.Filter = "Image files|*.png;*.bmp;*.jpg";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            if (!dlg.FileName.Equals(""))
            {
                ImageBrush brush = new ImageBrush();
                BitmapSource bitmapSource = new BitmapImage(new Uri(dlg.FileName,UriKind.Relative));
                BitmapUtils.ConvertBitmapSourceToOtherFormat(ref bitmapSource, PixelFormats.Bgr32);

                brush.ImageSource = bitmapSource;

                MainWindow sd = new MainWindow(brush, null, State.ManualMeasurement);
                sd.Show();
                this.Close();
            }
        }
        private void openEmpty_Click(object sender, RoutedEventArgs e)
        {
            MainWindow sd = new MainWindow(null, null, State.ManualMeasurement);
            sd.Show();
            this.Close();
        }

        private void openGoogleAPIPage_Click(object sender, RoutedEventArgs e)
        {
            changeGeoLocalisation();

            FindLocationInGoogleAPIWindow sd = new FindLocationInGoogleAPIWindow();
            sd.Show();
            this.Close();
        }


        private void changeGeoLocalisation()
        {
                String latS = lat + "";
                String lonS = lon + "";

                latS = latS.Replace(",", ".");
                lonS = lonS.Replace(",", ".");

                String path = Properties.Settings.Default.index;
                String indexHTML = File.ReadAllText(path);

                String pattern1 = @"var def_Lat = \d+;";
                String pattern2 = @"var def_Long = \d+;";
                String pattern3 = @"def_Zoom = \d+;";

                String replacement1 = @"var def_Lat = " + latS + ";";
                String replacement2 = @"var def_Long = " + lonS + ";";
                String replacement3 = @"def_Zoom = " + 14 + ";";

                Regex regex = new Regex(pattern1);
                String tmp = regex.Replace(indexHTML, replacement1);

                regex = new Regex(pattern2);
                String tmp2 = regex.Replace(tmp, replacement2);

                regex = new Regex(pattern3);
                String tmp3 = regex.Replace(tmp2, replacement3);

                File.WriteAllText(path, tmp3);  
        }

        private bool tryLocalisation()
        {
            var aa = 0;
            bool isOK = watcher.TryStart(false, TimeSpan.FromMilliseconds(2000));

            coord = watcher.Position.Location;
            if (isOK)
            {
                if (coord.IsUnknown != true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
