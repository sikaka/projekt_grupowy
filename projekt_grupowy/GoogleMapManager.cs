﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Windows;

namespace projekt_grupowy
{
    public class GoogleMapManager
    {
        public GeoCoordinate CenterGeo { get; set; }
        public GeoCoordinate LeftTopGeo { get; set; }
        public GeoCoordinate LeftBottomGeo { get; set; }
        public GeoCoordinate RightTopGeo { get; set; }
        public GeoCoordinate RightBottomGeo { get; set; }

        public int Zoom { get; set; }
        public Point CenterPixels { get; set; }
        public Point LeftBottomPixels { get; set; }
        public Point LeftTopPixels { get; set; }
        public Point RightTopPixels { get; set; }
        public Point RightBottomPixels { get; set; }

        private int TILE_SIZE = 256;

        public GoogleMapManager()
        {
        }
        public Point GeoToPixels(GeoCoordinate geo, int zoom)
        {
            int scale = 1 << zoom;
            Point worldCoordinate = Projection(geo, zoom);

            return new Point(
                Math.Floor(worldCoordinate.X * scale),
                Math.Floor(worldCoordinate.Y * scale));
        }
        private Point Projection(GeoCoordinate geo, int zoom)
        {
            var siny = Math.Sin(geo.Latitude * Math.PI / 180);

            // Truncating to 0.9999 effectively limits latitude to 89.189. This is
            // about a third of a tile past the edge of the world tile.
            siny = Math.Min(Math.Max(siny, -0.9999), 0.9999);

            return new Point(
                TILE_SIZE * (0.5 + geo.Longitude / 360),
                TILE_SIZE * (0.5 - Math.Log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
        }
    }
}
