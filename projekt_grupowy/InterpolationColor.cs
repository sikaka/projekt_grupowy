﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace projekt_grupowy
{
    public static class InterpolationColor
    {
        private static RGBConverter rgbConverter = new RGBConverter(-90, -40, new List<Color>() { Colors.Blue, Colors.Green, Colors.Red }); //ustawienie koorów i gwałtowności przejść w zależności od dB

        public static int skipDistance = 500; // w metrach
        public static double interpolationPower = 2;

        public static HeatMap generateHeatmap(SignalGrid sg, double opacity){
            int width = sg.xlength;
            int height = sg.ylength;

            PixelFormat pixelFormat = PixelFormats.Rgb24;
            int bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            int stride = bytesPerPixel * width;

            byte[] bits = new byte[sg.xlength * 3 * sg.ylength];

            double pixelWidth = sg.pixelWidth;

            if (pixelWidth != 0)
            {
                Parallel.For(0, sg.xlength, i =>
                {
                    for (int j = 0; j < sg.ylength; j++)
                    {
                        Point newP = new Point() {X = i, Y = j};
                        double sigValue = -90;
                        if (!sg.measureReference.ContainsKey(newP))
                        {
                            double numerator = 0, denominator = 0;

                            foreach (Point p in sg.measureReference.Keys)
                            {
                                double dist = CustomMath.distanceBetweenPoints(p, newP);
                                double distReal = dist*pixelWidth;
                                if (distReal / 100 < skipDistance)
                                {
                                    double denPart = (double)(1.0 / Math.Pow(distReal / 100, interpolationPower));
                                    double numPart = denPart * sg.measureReference[p];

                                    numerator += numPart;
                                    denominator += denPart;
                                }
                            }

                            sigValue = numerator/denominator;
                        }
                        else
                        {
                            sigValue = sg.measureReference[newP];
                        }

                        setpixel(ref bits, i, j, stride, rgbConverter.getColor(sigValue));
                    }
                });
            }

            return new HeatMap(height, width, bits, opacity);
        }

        private static void setpixel(ref byte[] bits, int x, int y, int stride, Color c)
        {
            bits[x * 3 + y * stride] = c.R;
            bits[x * 3 + y * stride + 1] = c.G;
            bits[x * 3 + y * stride + 2] = c.B;
        }
    }
}
