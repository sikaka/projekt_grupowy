﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace projekt_grupowy
{
    static class BitmapUtils
    {
        public static double dpiX = 96;
        public static double dpiY = 96;

        public static int getByteCount(int stride, int height)
        {
            return stride * height;
        }

        public static int getByteCount(PixelFormat pixelFormat, int height, int width)
        {
            return calcStride(pixelFormat, height, width) * height;
        }

        public static int calcStride(PixelFormat pixelFormat,int height, int width)
        {
            int bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            return bytesPerPixel * width; 
        }

        public static BitmapSource CreateBitmapSourceFromColor(Color color, int width, int height)
        {
            var pixelFormat = PixelFormats.Rgb24;
            int stride = calcStride(pixelFormat, height, width);
            byte[] bytes = new byte[getByteCount(stride,height)];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    bytes[i * 3 + j * stride] = color.R;
                    bytes[i * 3 + j * stride + 1] = color.G;
                    bytes[i * 3 + j * stride + 2] = color.B;
                }
            }
            return BitmapSource.Create(width, height, dpiX, dpiY, pixelFormat, null, bytes, stride);
        }

        public static BitmapSource CreateBitmapSourceFromBytes(byte[] bytes, int width, int height, PixelFormat pixelFormat)
        {
            int stride = calcStride(pixelFormat, height, width);
            return BitmapSource.Create(width, height, dpiX, dpiY, pixelFormat, null, bytes, stride);
        }

        public static void ConvertBitmapSourceToOtherFormat(ref BitmapSource bitmapSource, PixelFormat pixelFormat)
        {
            if (pixelFormat != bitmapSource.Format)
            {
                FormatConvertedBitmap newFormatedBitmapSource = new FormatConvertedBitmap();
                newFormatedBitmapSource.BeginInit();
                newFormatedBitmapSource.Source = bitmapSource;
                newFormatedBitmapSource.DestinationFormat = pixelFormat;
                newFormatedBitmapSource.EndInit();

                bitmapSource = newFormatedBitmapSource;
            }          
        }
    }
}
