﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace projekt_grupowy
{
    class RGBConverter
    {
        float minval;
        float maxval;
        List<Color> colors;
        ///
        public RGBConverter(float minval, float maxval, List<Color> colors)
        {
            this.colors = colors;
            this.maxval = maxval;
            this.minval = minval;
        }

        public Color getColor(double val)
        {
            if (val < minval)
            {
                val = minval;
            }
            if (val > maxval)
            {
                val = maxval;
            }
            if (val.Equals(Double.NaN))
            {
                val = minval;
            }
            Color kolorek = new Color();
            int max_index = colors.Count - 1;
            //val = val + minval;
            double v = ((val - minval) / (maxval - minval) * max_index);
            int i1 = (int)v;
            int i2 = Math.Min((int)v + 1, max_index);

            int r1 = colors[i1].R;
            int g1 = colors[i1].G;
            int b1 = colors[i1].B;

            int r2 = colors[i2].R;
            int g2 = colors[i2].G;
            int b2 = colors[i2].B;

            double f = v - i1;
            kolorek = Color.FromRgb((byte)(r1 + f * (r2 - r1)), (byte)(g1 + f * (g2 - g1)), (byte)(b1 + f * (b2 - b1)));

            return kolorek;
        }
    }
}
