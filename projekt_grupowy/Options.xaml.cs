﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for Options.xaml
    /// </summary>
    public partial class Options : Window
    {
        private bool isok = false;
        public bool accepted = false;
        private Dictionary<int, Color> getColor;
        public Options()
        {
            InitializeComponent();

            getColor = new Dictionary<int, Color>();
           
            getColor.Add(0, Colors.LightSeaGreen);
            getColor.Add(1, Colors.Blue);
            getColor.Add(2, Colors.DarkBlue);
            getColor.Add(3, Colors.LimeGreen);
            getColor.Add(4, Colors.Green);
            getColor.Add(5, Colors.DarkGreen);
            getColor.Add(6, Colors.Yellow);
            getColor.Add(7, Colors.Gold);
            getColor.Add(8, Colors.Orange);
            getColor.Add(9, Colors.DarkOrange);
            getColor.Add(10, Colors.OrangeRed);
            getColor.Add(11, Colors.Red);
            getColor.Add(12, Colors.Crimson);
            getColor.Add(13, Colors.MediumVioletRed);
          
            barLegend(Colors.Blue, Colors.Green, Colors.Red);

            isok = true;
        }

        private void barLegend(Color x, Color y, Color z)
        {
            //Usuwamy stara legende jezeli istnieje
            IEnumerable<Image> images = Legend.Children.OfType<Image>();
            Image oldLegend = images.FirstOrDefault<Image>(i => i.Tag.Equals("Legend"));

            if (oldLegend != null)
                Legend.Children.Remove(oldLegend);

            RGBConverter rgbConverter = new RGBConverter(-90, -40, new List<Color>() { x, y, z });

            var width = 600;
            var height = 18;
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Rgb24;
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            var stride = bytesPerPixel * width;
            double sigValue;

            byte[] bits = new byte[width * 3 * height];

            int minval = -90;
            int maxval = -40;
            int steps = width;

            double delta = (double)(maxval - minval) / steps;

            for (int i = 0; i < width; i++)
            {
                sigValue = minval + (double)(i) * delta;
                for (int j = 0; j < height; j++)
                {
                    setpixel(ref bits, i, j, stride, rgbConverter.getColor(sigValue));
                }
            }

            var bitmap = BitmapSource.Create(width, height, dpiX, dpiY, pixelFormat, null, bits, stride);

            Image bLegend = new Image();
            bLegend.Source = bitmap;
            bLegend.Opacity = 0.7;
            bLegend.Tag = "Legend";

            Legend.Children.Add(bLegend);
        }
        //REFAKTOR !!
        private static void setpixel(ref byte[] bits, int x, int y, int stride, Color c)
        {
            bits[x * 3 + y * stride] = c.R;
            bits[x * 3 + y * stride + 1] = c.G;
            bits[x * 3 + y * stride + 2] = c.B;
        }

        private void poor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isok == true)
            {
               barLegend(getColor[poor.SelectedIndex], getColor[good.SelectedIndex], getColor[best.SelectedIndex]);
            }
        }

        private void good_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isok == true)
            {
               barLegend(getColor[poor.SelectedIndex], getColor[good.SelectedIndex], getColor[best.SelectedIndex]);
            }
        }

        private void best_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isok == true)
            {
               barLegend(getColor[poor.SelectedIndex], getColor[good.SelectedIndex], getColor[best.SelectedIndex]);
            }
        }

        private void accept_click(object sender, RoutedEventArgs e)
        {
            accepted = true;
            this.Close();
        }

        private void reset_to_default_click(object sender, RoutedEventArgs e)
        {
            poor.SelectedIndex = 1;
            good.SelectedIndex = 4;
            best.SelectedIndex = 11;
        }
    }
}
