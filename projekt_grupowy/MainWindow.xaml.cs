﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Device.Location;
using System.Diagnostics;
using System.IO.Compression;
using System.Text;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private String build_date = "10/02/2016";

        // GPS section
        GoogleMapManager mapManager;
        State state;

        GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
        Dictionary<String, NetworkSurvey> Networks;

        double pixelWidth;

        List<UIPoint> pointsScale;

        List<UIPoint> globalmeasureReference;

        Line line;
        Color measureColor;

        bool defineScale = false;
        bool measurment = false;

        bool isScaleSet = false;

        bool toSave = false;

        bool clearSelectedMode = false;
        Ellipse lastSelected = null;

        bool tooltipsCleared = true;

        BackgroundWorker interp;

        ImageBrush backgroundImage;

        double hV, wV, lT;

        //Parametry
        double opacity_parametr = 0.7;
        bool visibility_of_measurment = true;
        double power_interpolation = 2;
        double power_extrapolation = 2;

        public MainWindow(ImageBrush image, GoogleMapManager mapManager, State state)
        {
            this.mapManager = mapManager;
            this.state = state;

            backgroundImage = image;
            SplashScreen splashScreen = new SplashScreen();
            splashScreen.Show();

            InitializeComponent();

            if (mapManager != null)
            {
                buttonSetScale.Visibility = Visibility.Hidden;
                buttonMakeMeasurment.Visibility = Visibility.Hidden;
            }
            else
            {
                buttonMakeGPSMeasurment.Visibility = Visibility.Hidden;
            }

            init();
            if (backgroundImage != null)
            {
                MainCanvas.Width = backgroundImage.ImageSource.Width;
                MainCanvas.Height = backgroundImage.ImageSource.Height;
                MainCanvas.Background = backgroundImage;
            }
            else
            {
                backgroundImage = new ImageBrush(BitmapUtils.CreateBitmapSourceFromColor((MainCanvas.Background as SolidColorBrush).Color, (int)MainCanvas.Width, (int)MainCanvas.Height));
            }
            NativeWifiApiScanner nwfa = NativeWifiApiScanner.Instance; //create instance

            System.Threading.Thread.Sleep(2000);
            splashScreen.Close();
            barLegend();
        }

        //TUTAJ DUZY REFAKTOR SIE PRZYDA!!
        private void barLegend()
        {
            //Usuwamy stara legende jezeli istnieje
            IEnumerable<Image> images = Legend.Children.OfType<Image>();
            Image oldLegend = images.FirstOrDefault<Image>(i => i.Tag.Equals("Legend"));

            if (oldLegend != null)
                Legend.Children.Remove(oldLegend);

            RGBConverter rgbConverter = new RGBConverter(-90, -40, new List<Color>() { Colors.Blue, Colors.Green, Colors.Red });

            var width = 345;
            var height = 18;
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Rgb24;
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            var stride = bytesPerPixel * width;
            double sigValue;

            byte[] bits = new byte[width * 3 * height];

            int minval = -90;
            int maxval = -40;
            int steps = width;

            double delta = (double)(maxval - minval) / steps;

            for (int i = 0; i < width; i++)
            {
                sigValue = minval + (double)(i) * delta;
                for (int j = 0; j < height; j++)
                {
                    setpixel(ref bits, i, j, stride, rgbConverter.getColor(sigValue));
                }
            }

            var bitmap = BitmapSource.Create(width, height, dpiX, dpiY, pixelFormat, null, bits, stride);

            Image bLegend = new Image();
            bLegend.Source = bitmap;
            bLegend.Opacity = opacity_parametr;
            bLegend.Tag = "Legend";

            Legend.Children.Add(bLegend);
        }
        //REFAKTOR !!
        private static void setpixel(ref byte[] bits, int x, int y, int stride, Color c)
        {
            bits[x * 3 + y * stride] = c.R;
            bits[x * 3 + y * stride + 1] = c.G;
            bits[x * 3 + y * stride + 2] = c.B;
        }

        public void init()
        {
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyDownEvent, new KeyEventHandler(keyDown), true);
            pointsScale = new List<UIPoint>();

            measureColor = Colors.Blue;

            globalmeasureReference = new List<UIPoint>();

            Networks = new Dictionary<string, NetworkSurvey>();

            interp = new BackgroundWorker();
            interp.WorkerReportsProgress = true;
            interp.DoWork += interpolateColor;
            interp.RunWorkerCompleted += interpolate_RunWorkerCompleted;
            interp.ProgressChanged += interpolate_ProgressChanged;
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            Info komunikat = new Info("Artur Kaliszewicz\nDamian Karpowicz\nKrzysztof Kanigowski\nŁukasz Sitkiewicz");
            komunikat.Title = "Authors";
            komunikat.ShowDialog();
        }

        private void Set_Scale(object sender, RoutedEventArgs e)
        {
            defineScale = true;
        }

        private void Make_Measurement(object sender, RoutedEventArgs e)
        {
            if (isScaleSet)
                measurment = true;
            else
            {
                Warning komunikat = new Warning("First set scale!");
                komunikat.ShowDialog();
            }    
        }

        private Ellipse findNearestMeasure(Point p)
        {
            List<Ellipse> measures = MainCanvas.Children.OfType<Ellipse>().Where(i => i.Tag.Equals("Measure")).ToList();

            double minDist = Double.MaxValue;
            Ellipse minDistPoint = null;

            foreach (Ellipse measure in measures)
            {
                double x = measure.Margin.Left + wV;
                double y = measure.Margin.Top + hV;

                double tmpDist = CustomMath.distanceBetweenPoints(p, new Point(x, y));
                if (tmpDist < minDist)
                {
                    minDist = tmpDist;
                    minDistPoint = measure;
                }
            }

            return minDistPoint;
        }

        private void Clear_MeasureLast(object sender, RoutedEventArgs e)
        {
            if (globalmeasureReference.Count > 0)
            {
                Point last = globalmeasureReference.Last().getPoint();
                MainCanvas.Children.Remove(findNearestMeasure(last));
                globalmeasureReference.RemoveAt(globalmeasureReference.Count - 1);

                foreach (NetworkSurvey ns in Networks.Values)
                {
                    ns.signalGrid.RemoveByPoint(last);
                }

                if (!measurment)
                {
                    if(globalmeasureReference.Count>0)
                        Generate();
                    else
                        apDataGrid.ItemsSource = null;
                }
            }
        }
        private void Make_GPSMeasurement(object sender, RoutedEventArgs e)
        {
            watcher.MovementThreshold = 10.0;
            watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(GeoPositionChanged);
            StartGPSMeasurementButton.Visibility = Visibility.Visible;
            StopGPSMeasurementButton.Visibility = Visibility.Visible;
        }

        private void StartGPSMeasurement(object sender, RoutedEventArgs e)
        {
            watcher.Start();         
        }

        private void StopGPSMeasurement(object sender, RoutedEventArgs e)
        {
            watcher.Stop();
            StartGPSMeasurementButton.Visibility = Visibility.Hidden;
            StopGPSMeasurementButton.Visibility = Visibility.Hidden;
            measurment = true;
        }

        private bool checkIfOutOfBounds(Point p)
        {
            if (p.X >= 0 && p.X <= MainCanvas.ActualWidth && p.Y >= 0 && p.Y <= MainCanvas.ActualHeight)
                return true;
            else
                return false;
        }

        private void GeoPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            double latitude = e.Position.Location.Latitude;
            double longitude = e.Position.Location.Longitude;
            Point gpsPoint = mapManager.GeoToPixels(new GeoCoordinate(latitude, longitude), mapManager.Zoom);

            Point leftBottom = mapManager.LeftBottomPixels;
            Point leftTop = mapManager.LeftTopPixels;

            Point gpsPoint_atCanvas = new Point(gpsPoint.X-leftBottom.X,gpsPoint.Y-leftTop.Y);

            pixelWidth = (156543.03392 * Math.Cos(latitude * Math.PI / 180) / Math.Pow(2, mapManager.Zoom)) * 100; //cm
            isScaleSet = true;

            if (checkIfOutOfBounds(gpsPoint_atCanvas))
            {
                UIPoint uip = AddMeasureToUI(gpsPoint_atCanvas);
                UpdateNetMeasureInfo(uip);
            }
            else
            {
                Warning komunikat = new Warning("Your position is out of map!");
                komunikat.ShowDialog();
            }
        }

        private void Clear_MeasureSelected(object sender, RoutedEventArgs e)
        {
            if (globalmeasureReference.Count > 0)
            {
                clearSelectedMode = true;
            }
        }

        private void Clear_MeasureAll(object sender, RoutedEventArgs e)
        {
            if (globalmeasureReference.Count > 0)
            {
                foreach (Ellipse measure in globalmeasureReference.Select(ui=>ui.getUIreference()))
                    MainCanvas.Children.Remove(measure);

                globalmeasureReference.Clear();

                foreach (NetworkSurvey ns in Networks.Values)
                {
                    ns.signalGrid.ClearMeasures();
                }
                apDataGrid.ItemsSource = null;
            }
        }

        private void Save_Project(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".eti"; // Default file extension
            dlg.FileName = "project"; // Default file name
            dlg.Filter = "Proj (.)|*.eti"; // Filter files by extension
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                BitmapSource bitmapSource = backgroundImage.ImageSource as BitmapSource;
                PixelFormat pixelFormat = bitmapSource.Format;

                int bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
                var stride = bytesPerPixel * bitmapSource.PixelWidth;
                byte[] bytes = new byte[stride * bitmapSource.PixelHeight];
                bitmapSource.CopyPixels(bytes, stride, 0);

                String pixelFormatS = new PixelFormatConverter().ConvertToString(pixelFormat);

                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(dlg.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
                GZipStream zipStream = new GZipStream(stream,CompressionMode.Compress);
                formatter.Serialize(zipStream, pixelWidth);
                formatter.Serialize(zipStream, isScaleSet);
                formatter.Serialize(zipStream, pixelFormatS);
                formatter.Serialize(zipStream, bitmapSource.PixelHeight);
                formatter.Serialize(zipStream, bitmapSource.PixelWidth);
                formatter.Serialize(zipStream, bytes);
                formatter.Serialize(zipStream, Networks);
                formatter.Serialize(zipStream, globalmeasureReference);
                formatter.Serialize(zipStream, opacity_parametr);
                formatter.Serialize(zipStream, visibility_of_measurment);
                formatter.Serialize(zipStream, power_interpolation);
                formatter.Serialize(zipStream, power_extrapolation);
                //formatter.Serialize(zipStream, build_date);
                zipStream.Close();
                stream.Close();

                toSave = false;
            }
        }

        private void Load_Project(object sender, RoutedEventArgs e)
        {
            if (toSave)
            {
                SessionSaveDialog();
            }

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".eti";
            dlg.Filter = "Proj (.bin)|*.eti";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            if (!dlg.FileName.Equals(""))
            {
                string ext = System.IO.Path.GetExtension(dlg.FileName);
                if (ext.Equals(".eti"))
                {
                    byte[] bytes = null;
                    int height, width;

                    IFormatter formatter = new BinaryFormatter();
                    Stream stream = new FileStream(dlg.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    GZipStream zipStream = new GZipStream(stream, CompressionMode.Decompress);

                    pixelWidth = (double)formatter.Deserialize(zipStream);
                    isScaleSet = (bool)formatter.Deserialize(zipStream);
                    String pixelFormatS = (String)formatter.Deserialize(zipStream);
                    height = (int)formatter.Deserialize(zipStream);
                    width = (int)formatter.Deserialize(zipStream);
                    bytes = (byte[])formatter.Deserialize(zipStream);
                    Dictionary<String, NetworkSurvey> nets = (Dictionary<String, NetworkSurvey>)formatter.Deserialize(zipStream);
                    List<UIPoint> measureRef = (List<UIPoint>)formatter.Deserialize(zipStream);
                    opacity_parametr = (double)formatter.Deserialize(zipStream);
                    visibility_of_measurment = (bool)formatter.Deserialize(zipStream);
                    power_interpolation = (double)formatter.Deserialize(zipStream);
                    power_extrapolation = (double)formatter.Deserialize(zipStream);
                    //String date_of_program_build = (String)formatter.Deserialize(zipStream);
                    zipStream.Close();
                    stream.Close();

                    PixelFormatConverter pconverter = new PixelFormatConverter();
                    PixelFormat pixelFormat = (PixelFormat)pconverter.ConvertFrom(pixelFormatS);

                    BitmapSource bitmapSource = BitmapUtils.CreateBitmapSourceFromBytes(bytes, width, height, pixelFormat);

                    backgroundImage = new ImageBrush(bitmapSource);
                    MainCanvas.Background = backgroundImage;
                    Networks.Clear();
                    Networks = nets;
                    globalmeasureReference.Clear();
                    globalmeasureReference = measureRef;
                    MainCanvas.Height = height;
                    MainCanvas.Width = width;
                    setObjectsScale();
                    setDataGrid();
                    redrawMeasurePoints();
                    barLegend();
                    setMeasuresVisibility(visibility_of_measurment);
                }
            }
        }

        private void SessionSaveDialog()
        {
            CheckExit sdf = new CheckExit();
            sdf.ShowDialog();

            if (sdf.yes == true)
            {
                Save_Project(this, null);
            }
        }

        private void redrawMeasurePoints()
        {
            List<UIElement> measure_list = MainCanvas.Children.Cast<FrameworkElement>().Where(c => c.Tag.Equals("Measure")).ToList<UIElement>();
            foreach (UIElement ui_element in measure_list)
            {
                MainCanvas.Children.Remove(ui_element);
            }
            foreach (UIPoint p in globalmeasureReference)
            {
                Ellipse pref = p.getUIreference();
                pref.Tag = "Measure";
                Canvas.SetZIndex(pref,3);
                MainCanvas.Children.Add(pref);
            }
        }

        private void clearFromCanvas(List<UIPoint> points)
        {
            foreach (UIPoint elpsTmp in points)
                MainCanvas.Children.Remove(elpsTmp.getUIreference());
            points.Clear();
        }

        private void clearRuler()
        {
            clearFromCanvas(pointsScale);
            MainCanvas.Children.Remove(line);
        }

        private void MainCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (mapManager == null)
            {
                if (defineScale)
                {
                    Point mouse = Mouse.GetPosition(MainCanvas);
                    UIPoint p = new UIPoint((int)mouse.X, (int)mouse.Y, wV, hV, measureColor, 1);

                    if (pointsScale.Count == 2)
                    {
                        clearRuler();
                    }

                    MainCanvas.Children.Add(p.getUIreference());
                    pointsScale.Add(p);

                    if (pointsScale.Count == 2)
                    {
                        line = new Line();
                        line.Stroke = Brushes.Black;
                        line.StrokeThickness = lT;

                        UIPoint pointA = pointsScale.First();

                        line.X1 = pointA.X;
                        line.Y1 = pointA.Y;

                        UIPoint pointB = pointsScale.Last();

                        line.X2 = pointB.X;
                        line.Y2 = pointB.Y;

                        MainCanvas.Children.Add(line);

                        SetScaleForPixels();

                        toSave = true;
                    }
                }
                else if (clearSelectedMode)
                {
                    double x = lastSelected.Margin.Left + wV;
                    double y = lastSelected.Margin.Top + hV;

                    Point last = new Point((int)x, (int)y);
                    MainCanvas.Children.Remove(lastSelected);

                    int pos = globalmeasureReference.Select(uip => uip.getPoint()).ToList().IndexOf(last);
                    if (pos != -1)
                        globalmeasureReference.RemoveAt(pos);

                    foreach (NetworkSurvey ns in Networks.Values)
                    {
                        ns.signalGrid.RemoveByPoint(last);
                    }

                    if (!measurment)
                    {
                        if (globalmeasureReference.Count > 0)
                            Generate();
                        else
                            apDataGrid.ItemsSource = null;
                    }

                    clearSelectedMode = false;
                }
                else if (measurment)
                {
                    Point mouse = Mouse.GetPosition(MainCanvas);
                    UIPoint uip = AddMeasureToUI(mouse);
                    UpdateNetMeasureInfo(uip);
                }
            }
        }

        private UIPoint AddMeasureToUI(Point p)
        {
            UIPoint uip = new UIPoint((int)p.X, (int)p.Y, wV, hV, measureColor, 1);
            Ellipse pref = uip.getUIreference();
            pref.Tag = "Measure";
            Canvas.SetZIndex(pref, 3);
            MainCanvas.Children.Add(pref);

            return uip;
        }

        private void UpdateNetMeasureInfo(UIPoint uipoint)
        {
            Point p = uipoint.getPoint();
            List<Network> Nets = NativeWifiApiScanner.Instance.ScanedNetworks;
            //Aktualizacja Networks
            foreach (Network net in Nets)
            {
                if (Networks.ContainsKey(net.Bssid))
                {
                    Networks[net.Bssid].network.SignalStrength = net.SignalStrength;
                }
                else
                {
                    SignalGrid sg = new SignalGrid((int)MainCanvas.Width, (int)MainCanvas.Height) { pixelWidth = this.pixelWidth };
                    foreach (Point pt in globalmeasureReference.Select(uip => uip.getPoint()))
                    {
                        sg.Add(-90, (int)pt.X, (int)pt.Y);
                    }
                    NetworkSurvey ns = new NetworkSurvey(net, sg);
                    Networks.Add(net.Bssid, ns);
                }
            }

            List<NetworkSurvey> networkSurveys = Networks.Values.ToList<NetworkSurvey>();

            networkSurveys.Where(ns => !Nets.Contains(ns.network)).Select(ns => ns.network.SignalStrength = -90);

            foreach (NetworkSurvey ns in networkSurveys)
            {
                ns.signalGrid.Add((float)ns.network.SignalStrength, (int)p.X, (int)p.Y);
            }

            if (!globalmeasureReference.Contains(uipoint))
                globalmeasureReference.Add(uipoint);

            toSave = true;
        }

        private void interpolateColor(object sender, DoWorkEventArgs e)
        {
            int progressstep = (int)Math.Ceiling(100.0 / Networks.Count);

            foreach (NetworkSurvey ns in Networks.Values)
            {
                List<Point> extraPoints = Extrapolation.calculateValues(ns.signalGrid, ns.network.Freq);
                HeatMap heatmap = InterpolationColor.generateHeatmap(ns.signalGrid, opacity_parametr);
                ns.heatmap = heatmap;

                //usuniecie dodanych przez ekstrapolacje punktow "sztucznych"
                foreach (Point exP in extraPoints)
                    ns.signalGrid.RemoveByPoint(exP);

                (sender as BackgroundWorker).ReportProgress(progressstep);
            }
        }

        void interpolate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgbar.Value += e.ProgressPercentage;
        }

        private void interpolate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pgbar.Value = 0;
            setDataGrid();
        }

        private void setDataGrid()
        {
            if (typeCombo.SelectedIndex == 0)
            {
                apDataGrid.ItemsSource = Networks.Values.Select(ns => ns.network);
                apDataGrid.SelectionMode = DataGridSelectionMode.Extended;
            }
            else
            {
                apDataGrid.ItemsSource = Networks.Values.Select(ns => ns.network).GroupBy(net => net.Ssid).Select(nets => nets.First());
                apDataGrid.Columns[4].Visibility = Visibility.Hidden;
                apDataGrid.SelectionMode = DataGridSelectionMode.Single;
            }
            if (apDataGrid.Items.Count != 0)
            {
                apDataGrid.Columns[1].Visibility = Visibility.Hidden;
                apDataGrid.Columns[5].Visibility = Visibility.Hidden;
                apDataGrid.Columns[6].Visibility = Visibility.Hidden;
                apDataGrid.Columns[7].Visibility = Visibility.Hidden;
                apDataGrid.Columns[8].Visibility = Visibility.Hidden;
                apDataGrid.Columns[9].Visibility = Visibility.Hidden;
            }
        }

        private void MainCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            toGenerate();
        }
        private void buttonGenerateHeatMap_Click(object sender, RoutedEventArgs e)
        {
            toGenerate();
        }

        private void toGenerate()
        {
            if (measurment)
            {
                Generate();
            }
            else
            {
                Warning komunikat = new Warning("First make measurments!");
                komunikat.ShowDialog();
            }
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SetScaleForPixels();
            }
        }

        private void SetScaleForPixels()
        {
            if (defineScale)
            {
                defineScale = false;
                ScaleDialog sd = new ScaleDialog();
                sd.ShowDialog();

                if (sd.repeat)
                {
                    clearRuler();
                }
                else
                {
                    double pixelDistance = CustomMath.distanceBetweenPoints(pointsScale.First().getPoint(), pointsScale.Last().getPoint());
                    clearRuler();
                    pixelWidth = sd.scale / pixelDistance * 100; //cm
                    isScaleSet = true;
                }
            }
        }

        private void removeOldHeatmap()
        {
            IEnumerable<Image> images = MainCanvas.Children.OfType<Image>();
            Image oldHeatmap = images.FirstOrDefault<Image>(i => i.Tag.Equals("Heatmap"));

            if (oldHeatmap != null)
                MainCanvas.Children.Remove(oldHeatmap);
        }

        private void Export_Heatmap(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".bmp"; // Default file extension
            dlg.FileName = "heatmap"; // Default file name
            dlg.Filter = "Image Files (*.bmp, *.jpg)|*.bmp;*.jpg"; // Filter files by extension
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                Rect bounds = VisualTreeHelper.GetDescendantBounds(MainCanvas);
                double dpi = 96d;
                RenderTargetBitmap rtb = new RenderTargetBitmap((int)bounds.Width, (int)bounds.Height, dpi, dpi, System.Windows.Media.PixelFormats.Default);

                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext dc = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(MainCanvas);
                    dc.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
                }

                rtb.Render(dv);

                BitmapEncoder bmpEncoder = new BmpBitmapEncoder();
                bmpEncoder.Frames.Add(BitmapFrame.Create(rtb));

                MemoryStream ms = new MemoryStream();

                bmpEncoder.Save(ms);
                ms.Close();

                File.WriteAllBytes(dlg.FileName, ms.ToArray());
            }
        }

        private void Exit_Window(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void setMeasuresVisibility(bool visibility)
        {
            Visibility vis;
            if (visibility)
            {
                vis = Visibility.Visible;
            }
            else
            {
                vis = Visibility.Hidden;
            }

            List<UIElement> measure_list = MainCanvas.Children.Cast<FrameworkElement>().Where(c => c.Tag.Equals("Measure")).ToList<UIElement>();
            foreach (UIElement ui_element in measure_list)
            {
                ui_element.Visibility = vis;
            }

            visibility_of_measurment = visibility;
        }

        private void Set_Parameters(object sender, RoutedEventArgs e)
        {
            ParameterWindow pd = new ParameterWindow(opacity_parametr, Extrapolation.extrapolation_power, InterpolationColor.interpolationPower, visibility_of_measurment);
            pd.ShowDialog();

            if (pd.accepted == true)
            {
                double op = (pd.slider_opacity.Value / 100);

                if (opacity_parametr != op)
                {
                    opacity_parametr = op;
                    barLegend();
                    foreach (KeyValuePair<string, NetworkSurvey> nk in Networks)
                    {
                        nk.Value.heatmap.Opacity = op;
                    }
                }

                if ((pd.slider_interpolation.Value != power_interpolation) || (pd.slider_extrapolation.Value != power_extrapolation))
                {
                    InterpolationColor.interpolationPower = pd.slider_interpolation.Value;
                    Extrapolation.extrapolation_power = pd.slider_extrapolation.Value;
                    power_interpolation = pd.slider_interpolation.Value;
                    power_extrapolation = pd.slider_extrapolation.Value;

                    Generate();
                }

                if (visibility_of_measurment != pd.visible_measurment)
                {
                    setMeasuresVisibility(pd.visible_measurment);
                }

                pd.accepted = false;
                apDataGrid.SelectedIndex = -1;
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            setObjectsScale();
        }

        private void setObjectsScale()
        {
            hV = 5 / (viewBox.ActualHeight / MainCanvas.Height);
            wV = 5 / (viewBox.ActualWidth / MainCanvas.Width);
            lT = 2 / (viewBox.ActualWidth / MainCanvas.Width);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (toSave)
            {
                SessionSaveDialog();
            }
            StartWindow sw = new StartWindow();
            sw.Show();
        }

        private HeatMap heatmapSourceMerge(List<HeatMap> heatmaps)
        {
            int size = heatmaps[0].getByteCount();
            byte[] pixelByte1 = new byte[size];
            byte[] pixelByte2 = new byte[size];

            Array.Copy(heatmaps[0].getBytes(), pixelByte1, size);

            foreach (HeatMap heatmap in heatmaps.Skip(1))
            {
                Array.Copy(heatmap.getBytes(), pixelByte2, size);
                for (int i = 0; i < size; i += 3)
                {
                    int v1 = pixelByte1[i] * 1000000 + pixelByte1[i + 1] * 1000 + pixelByte1[i + 2];
                    int v2 = pixelByte2[i] * 1000000 + pixelByte2[i + 1] * 1000 + pixelByte2[i + 2];

                    if (v2 > v1)
                    {
                        pixelByte1[i] = pixelByte2[i];
                        pixelByte1[i + 1] = pixelByte2[i + 1];
                        pixelByte1[i + 2] = pixelByte2[i + 2];
                    }
                }
            }
            return new HeatMap(heatmaps[0].height, heatmaps[0].width, pixelByte1, opacity_parametr);
        }

        private void clearTooltips()
        {
            if (!tooltipsCleared)
            {
                foreach (UIPoint uip in globalmeasureReference)
                {
                    uip.getUIreference().ToolTip = null;
                }
                tooltipsCleared = true;
            }
        }

        private void setTooltips(NetworkSurvey net)
        {
            foreach (UIPoint uip in globalmeasureReference)
            {
                Point p = uip.getPoint();
                Dictionary<Point, float> netPoints = net.signalGrid.measureReference;
                if (netPoints.ContainsKey(p))
                {
                    float signal = netPoints[p];
                    uip.getUIreference().ToolTip = signal + " dBm";
                }
                else
                {
                    uip.getUIreference().ToolTip = "-90 dBm";
                }
            }
            tooltipsCleared = false;
        }

        private void apDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg.SelectedIndex == -1)
            {
                removeOldHeatmap();
                return;
            }

            Image heatmap = new Image();

            List<HeatMap> heatmaps = new List<HeatMap>();

            if (typeCombo.SelectedIndex == 1)
            {
                Network net = dg.SelectedItem as Network;
                List<NetworkSurvey> nets = Networks.Values.Where(n => n.network.Ssid.Equals(net.Ssid)).ToList<NetworkSurvey>();

                foreach (NetworkSurvey n in nets)
                {
                    heatmaps.Add(n.heatmap);
                }

                clearTooltips();
            }
            else
            {
                foreach (Network n in dg.SelectedItems)
                {
                    NetworkSurvey netS = Networks.Values.FirstOrDefault(ns => ns.network.Equals(n));
                    heatmaps.Add(netS.heatmap);

                    if (dg.SelectedItems.Count == 1)
                    {
                        setTooltips(netS);
                    }
                    else
                    {
                        clearTooltips();
                    }
                }
            }

            if (heatmaps.Count > 1)
            {
                heatmap = heatmapSourceMerge(heatmaps).getImage();
            }
            else
            {
                heatmap = heatmaps.First().getImage();
            }

            heatmap.Opacity = heatmaps.First().Opacity;
            heatmap.Tag = "Heatmap";
            removeOldHeatmap();

            MainCanvas.Children.Add(heatmap);
        }

        private void typeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Networks != null && Networks.Count > 0)
            {
                apDataGrid.SelectedIndex = -1;
                setDataGrid();
            }
            clearTooltips();
        }

        private void Generate()
        {
            measurment = false;
            interp.RunWorkerAsync();
        }

        private void Open_Tutorial(object sender, RoutedEventArgs e)
        {
            var programPath = Properties.Settings.Default.tutor;
            if (!File.Exists(programPath))
            {
                Warning komunikat = new Warning("Error! Html file not found!");
                komunikat.ShowDialog();
            }
            else
            {
                Process proc = new Process();
                proc.StartInfo.FileName = programPath;
                proc.Start();
            }
        }

        private void Set_Options(object sender, RoutedEventArgs e)
        {
            Options window = new Options();
            window.ShowDialog();
        }

        private void export_to_CSV__menu_clicked(object sender, RoutedEventArgs e)
        {
            if (Networks.Count != 0)
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".csv"; // Default file extension
                dlg.FileName = "exported from WeyeFeyeMapper"; // Default file name
                dlg.Filter = "CSV|*.csv"; // Filter files by extension
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    var csv = new StringBuilder();
                    var startLine = string.Format("{0};{1};{2};{3};{4}", "Position X", "Position Y", "Ssid", "Bssid", "Signal strength [dBm]");
                    csv.AppendLine(startLine);
                    foreach (NetworkSurvey ns in Networks.Values)
                    {
                        foreach (KeyValuePair<Point, float> measurment in ns.signalGrid.measureReference)
                        {
                            var newLine = string.Format("{0};{1};{2};{3};{4}", measurment.Key.X, measurment.Key.Y, ns.network.Ssid, ns.network.Bssid, measurment.Value);
                            csv.AppendLine(newLine);
                        }         
                    }

                    File.WriteAllText(dlg.FileName, csv.ToString());
                }
            }
            else
            {
                Warning komunikat = new Warning("No signal measurments!");
                komunikat.ShowDialog();
            }

        }
    
        private void MainCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (clearSelectedMode)
            {
                Point mouse = Mouse.GetPosition(MainCanvas);
                Ellipse measure = findNearestMeasure(mouse);

                if (lastSelected != measure)
                {
                    if (lastSelected != null)
                    {
                        lastSelected.Fill = new SolidColorBrush(measureColor);
                    }
                    measure.Fill = Brushes.Red;
                    lastSelected = measure;
                }
            }
        }

        private void Open_Version_Info(object sender, RoutedEventArgs e)
        {
            Info komunikat = new Info(build_date);
            komunikat.Title = "Version";
            komunikat.ShowDialog();
        }
    }
}
