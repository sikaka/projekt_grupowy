﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace projekt_grupowy
{
    static class Extrapolation
    {
        public static double extrapolation_power = 2;

        public static List<Point> calculateValues(SignalGrid sg, int freq)
        {
            List<Point> extraPoints = new List<Point>();

            List<Point> measure = sg.measureReference.Keys.ToList<Point>();

            Point left_up, left_down, right_up, right_down;
            double dist;
            double dist_min_left_up = double.MaxValue;
            double dist_min_left_down = double.MaxValue;
            double dist_min_right_up = double.MaxValue;
            double dist_min_right_down = double.MaxValue;

            left_up = new Point() { X = 0, Y = 0 };
            left_down = new Point() { X = 0, Y = sg.ylength - 1 };
            right_up = new Point() { X = sg.xlength - 1, Y = 0 };
            right_down = new Point() { X = sg.xlength - 1, Y = sg.ylength - 1 };

            //dodajemy te punkty do listy, ktore beda dodane przez ekstrapolacje(jezeli juz jest jakis pomiar w tych punktach, to nie bedzie on dodany)
            if (!measure.Contains(left_up))
            {
                extraPoints.Add(left_up);
            }
            if (!measure.Contains(left_down))
            {
                extraPoints.Add(left_down);
            }
            if (!measure.Contains(right_down))
            {
                extraPoints.Add(right_down);
            }
            if (!measure.Contains(right_up))
            {
                extraPoints.Add(right_up);
            }

            Point left_up_min_found, left_down_min_found, right_up_min_found, right_down_min_found;
            left_up_min_found = new Point() { X = 0, Y = 0 };
            left_down_min_found = new Point() { X = 0, Y = 0 };
            right_up_min_found = new Point() { X = 0, Y = 0 };
            right_down_min_found = new Point() { X = 0, Y = 0 };

            foreach (Point p in measure)
            {
                dist = CustomMath.distanceBetweenPoints(p, left_up);

                if (dist_min_left_up > dist)
                {
                    dist_min_left_up = dist;
                    left_up_min_found = p;
                }

                dist = CustomMath.distanceBetweenPoints(p, left_down);

                if (dist_min_left_down > dist)
                {
                    dist_min_left_down = dist;
                    left_down_min_found = p;
                }

                dist = CustomMath.distanceBetweenPoints(p, right_up);

                if (dist_min_right_up > dist)
                {
                    dist_min_right_up = dist;
                    right_up_min_found = p;
                }

                dist = CustomMath.distanceBetweenPoints(p, right_down);

                if (dist_min_right_down > dist)
                {
                    dist_min_right_down = dist;
                    right_down_min_found = p;
                }
            }

            //left_up
            sg.AddByPoint(getPointExtrValue(left_up_min_found, dist_min_left_up, freq, sg), left_up);

            //left_down
            sg.AddByPoint(getPointExtrValue(left_down_min_found, dist_min_left_down, freq, sg), left_down);

            //right_up
            sg.AddByPoint(getPointExtrValue(right_up_min_found, dist_min_right_up, freq, sg), right_up);

            //right_down
            sg.AddByPoint(getPointExtrValue(right_down_min_found, dist_min_right_down, freq, sg), right_down);

            return extraPoints;
        }

        private static float getPointExtrValue(Point p, double dist, int freq, SignalGrid sg)
        {
            double temp1;
            double temp;
            int min_val = -90;
            double multiplier = 10 * extrapolation_power;
            double consant_value = multiplier * Math.Log10(4 * Math.PI / (2.99792458 * 100));
            double signal;

            signal = Math.Abs((double)sg.measureReference[p]);
            temp1 = (signal - multiplier * Math.Log10(freq) - consant_value) / multiplier;
            temp1 = Math.Pow(10, temp1);
            temp = -(multiplier * Math.Log10((sg.pixelWidth * dist / 100) + temp1) + multiplier * Math.Log10(freq) + consant_value);

            temp = temp < min_val ? min_val : temp;

            return (float)temp;
        }
    }
}
