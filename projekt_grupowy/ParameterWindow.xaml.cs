﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for ParameterWindow.xaml
    /// </summary>
    public partial class ParameterWindow : Window
    {

        public bool accepted = false;
        double Opacity;
        double Extra;
        double Inter;
        public bool visible_measurment = false;
        

        public ParameterWindow(double Opacity, double Extra, double Inter, bool visibility)
        {
            InitializeComponent();
            this.Opacity = Opacity;
            this.Extra = Extra;
            this.Inter = Inter;
            this.visible_measurment = visibility;

            if (visibility == true)
            {
                measure_visible_points_checkbox.SetCurrentValue(CheckBox.IsCheckedProperty, true);
            }
            else
            {
                measure_visible_points_checkbox.SetCurrentValue(CheckBox.IsCheckedProperty, false);
            }


            opacity_textbox.Text = Convert.ToString((double)Opacity);
            slider_opacity.Value = Opacity*100;

            interpolation_textbox.Text = Convert.ToString(Inter);
            slider_interpolation.Value = Inter;

            extrapolation_textbox.Text = Convert.ToString(Extra);
            slider_extrapolation.Value = Extra;
        }

        private void silder_opacity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            opacity_textbox.Text = Convert.ToString(slider_opacity.Value);
        }

        private void opacity_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double opacity = Convert.ToDouble(opacity_textbox.Text);

                if (opacity > 100)
                {
                    opacity = 100;
                    opacity_textbox.Text = Convert.ToString(opacity);
                    opacity_textbox.CaretIndex = opacity_textbox.Text.Count();
                }
                if (opacity < 0)
                {
                    opacity = 0;
                    opacity_textbox.Text = Convert.ToString(opacity);
                    opacity_textbox.CaretIndex = opacity_textbox.Text.Count();
                }

                slider_opacity.Value = opacity;
                opacity_textbox.CaretIndex = opacity_textbox.Text.Count();
            }
            catch (Exception ee)
            {
                opacity_textbox.Text = "40";
                slider_opacity.Value = 0;
                opacity_textbox.CaretIndex = opacity_textbox.Text.Count();
            }
        }

        private void slider_interpolation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            interpolation_textbox.Text = Convert.ToString(slider_interpolation.Value);
        }

        private void interpolation_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double interpolation = Convert.ToDouble(interpolation_textbox.Text);
                

                if (interpolation > 3.5)
                {
                    interpolation = 3.5;
                    interpolation_textbox.Text = Convert.ToString(interpolation);
                    interpolation_textbox.CaretIndex = interpolation_textbox.Text.Count();
                }
                if (interpolation < 2)
                {
                    interpolation = 2;
                    interpolation_textbox.Text = Convert.ToString(interpolation);
                    interpolation_textbox.CaretIndex = interpolation_textbox.Text.Count();
                }
                slider_interpolation.Value = interpolation;
                interpolation_textbox.CaretIndex = interpolation_textbox.Text.Count();
            }
            catch (Exception ee)
            {
                interpolation_textbox.Text = "2";
                slider_interpolation.Value = 2;
                interpolation_textbox.CaretIndex = interpolation_textbox.Text.Count();
            }
        }

        private void slider_extrapolation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            extrapolation_textbox.Text = Convert.ToString(slider_extrapolation.Value);
        }

        private void extrapolation_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double extrapolation = Convert.ToDouble(extrapolation_textbox.Text);
                

                if (extrapolation > 3.5)
                {
                    extrapolation = 3.5;
                    extrapolation_textbox.Text = Convert.ToString(extrapolation);
                    extrapolation_textbox.CaretIndex = extrapolation_textbox.Text.Count();
                }
                if (extrapolation < 2)
                {
                    extrapolation = 2;
                    extrapolation_textbox.Text = Convert.ToString(extrapolation);
                    extrapolation_textbox.CaretIndex = extrapolation_textbox.Text.Count();
                }
                slider_extrapolation.Value = extrapolation;
                extrapolation_textbox.CaretIndex = extrapolation_textbox.Text.Count();
            }
            catch (Exception ee)
            {
                extrapolation_textbox.Text = "2";
                slider_extrapolation.Value = 2;
                extrapolation_textbox.CaretIndex = extrapolation_textbox.Text.Count();
            }
        }

        private void accept_button_Click(object sender, RoutedEventArgs e)
        {
            accepted = true;
            this.Close();
        }

        private void reset_to_default_button_Click(object sender, RoutedEventArgs e)
        {
            opacity_textbox.Text = "50";
            slider_opacity.Value = 50;

            interpolation_textbox.Text = "2.5";
            slider_interpolation.Value = 2.5;

            extrapolation_textbox.Text = "2.5";
            slider_extrapolation.Value = 2.5;
        }

        private void measure_visible_points_checkbox_Checked(object sender, RoutedEventArgs e)
        {
            visible_measurment = true;
        }

        private void measure_visible_points_checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            visible_measurment = false;
        }
    }
}